---
- name: main | Manage borg apt repository
  apt_repository:
    repo: ppa:costamagnagianfranco/borgbackup

- name: main | Manage needed packages
  apt:
    name:
    - borgbackup  # For doing the heavy lifting
    - apg         # For generating a typable passwords as repository passphrase
    state: present

- name: main | Create directory for backup scripts
  file:
    dest: "{{ borgbackup_script_path }}"
    state: directory
    mode: 0700

- name: main | Generate ssh keys for each backup profile
  command: "ssh-keygen -q -b 4096 -C 'backup-{{ inventory_hostname }}-{{ item.key }}' -N '' -f '~/.ssh/id_rsa-backup-{{ item.key }}'"
  args:
    creates: "~/.ssh/id_rsa-backup-{{ item.key }}"
  with_dict: "{{ borgbackup_profiles }}"
  loop_control: {label: "{{ item.key }}"}
  when: (borgbackup_profiles is defined)

- name: main | Generate passphrase for each backup profile
  vars:
    keyfile: "{{ borgbackup_script_path }}/{{ item.key }}.key"
  shell: |
    touch {{ keyfile }}
    chmod 0600 {{ keyfile }}
    apg -m 64 -n 1 -q > {{ keyfile }}
  args:
    creates: "{{ keyfile }}"
    warn: no  # Don't warn about using touch
  with_dict: "{{ borgbackup_profiles }}"
  loop_control: {label: "{{ item.key }}"}
  when: (borgbackup_profiles is defined)

- name: main | Fetch borg public keys
  fetch:
    src: "~/.ssh/id_rsa-backup-{{ item.key }}.pub"
    dest: "borgbackup/id_rsa-backup-{{ inventory_hostname }}-{{ item.key }}.pub"
    flat: yes
  with_dict: "{{ borgbackup_profiles }}"
  loop_control: {label: "{{ item.key }}"}
  when: (borgbackup_profiles is defined)

- name: main | Import borg storage tasks for hosts that are in that group
  import_tasks: storage.yml
  when: "'backup-storage' in group_names"

- name: main | Create backup scripts
  template:
    src: backup_profile.j2
    dest: "~/backup/{{ item.key }}.sh"
    mode: 0700
  with_dict: "{{ borgbackup_profiles }}"
  loop_control: {label: "{{ item.key }}"}
  when: (borgbackup_profiles is defined)

- name: main | Add known hosts entries
  known_hosts:
    name: "{{ item.name }}"
    key: "{{ item.key }}"
  with_items: "{{ borgbackup_known_hosts }}"
  loop_control: {label: "{{ item.name }}"}

- name: main | Initialize backup repositories
  shell: |
    export BORG_PASSPHRASE="$(cat "{{ borgbackup_script_path }}/{{ item.key }}.key")"
    export BORG_REPO="{{ borgbackup_repo_user }}@{{ borgbackup_repo_host }}:~/repositories/{{ inventory_hostname }}-{{ item.key }}"
    export BORG_RSH="ssh -i ~/.ssh/id_rsa-backup-{{ item.key }} -p {{ borgbackup_repo_port }}"
    borg init -e repokey
  register: borgbackup_init_repo
  changed_when: borgbackup_init_repo.rc == 0
  failed_when: borgbackup_init_repo.rc != 0 and 'repository already exists' not in borgbackup_init_repo.stderr
  with_dict: "{{ borgbackup_profiles }}"
  loop_control: {label: "{{ item.key }}"}
  when: (borgbackup_profiles is defined)

- name: main | Schedule backup profiles
  cron:
    name: "Borgbackup profile {{ item.key }}"
    job: "{{ borgbackup_script_path }}/{{ item.key }}.sh |tee -a {{ borgbackup_script_path }}/{{ item.key }}.log"
    minute: "{{ (59 | random(seed=inventory_hostname)) if item.value.cron.get('minute', '*') == 'random' else item.value.cron.get('minute', '*') }}"
    hour: "{{ (23 | random(seed=inventory_hostname)) if item.value.cron.get('hour', '*') == 'random' else item.value.cron.get('hour', '*') }}"
    day: "{{ (31 | random(start=1, seed=inventory_hostname)) if item.value.cron.get('day', '*') == 'random' else item.value.cron.get('day', '*') }}"
    weekday: "{{ (6 | random(seed=inventory_hostname)) if item.value.cron.get('weekday', '*') == 'random' else item.value.cron.get('weekday', '*') }}"
    month: "{{ (12 | random(start=1, seed=inventory_hostname)) if item.value.cron.get('month', '*') == 'random' else item.value.cron.get('month', '*') }}"
  with_dict: "{{ borgbackup_profiles }}"
  loop_control: {label: "{{ item.key }}"}
  when: (borgbackup_profiles is defined and item.value.cron is defined)

