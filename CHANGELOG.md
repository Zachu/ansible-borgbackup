# Changelog
All notable changes to this project are tried to document in this file. This changelog tries to follow guidelines of
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and it also tries really hard to adhere to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2018-11-08
### Added
- Installing borgbackup ppa (`ppa:costamagnagianfranco/borgbackup`)
- Installing borgbackup
- Installing apg for generating typable passphrases for borg
- Generating ssh keys for each backup profile
- Creating a unprivileged user for storing borg backups
- Allowing a limited access for the generated ssh keys to use borg on a single directory on a storage host
- Creating backup profile scripts
- Initializing the backup profile repositories
- Scheduling the backup profiles
