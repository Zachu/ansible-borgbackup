BorgBackup
==========

This Ansible role is for managing hosts that use BorgbBckup for backing
themselves up, and also for managing hosts that are used to store the backups
using BorgBackup.

This role is meant to be as automatic as possible. Just put the suitable values
in and let this role manage generating passphrases and sharing public keys.

## TODO/Techical Debt

- Documentation of usable variables
- Documentation of inner workings
- Finish up meta/main.yml
- Change storage user and repo path configurable

## Why would you do this?

For real, I haven't even searched Ansible Galaxy whether this sort of role
already exists. Probably it does! This is mainly created as my own hobby, for 
my own needs, and most importantly as a way of learning Ansible. Of course
everyone can use it if it suits their needs!
